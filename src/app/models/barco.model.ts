export class Barco {
  // DATABASE VARS
  codigo: number;
  estado: number;
  nombre: string;
  categoria: number;
  descripcion: string;
  capacidad: number;
  imagen: string;
  tipoImagen: string;
  nombreImagen: string;
  logo: string;
  tipoLogo: string;
  nombreLogo: string;
  tipoBarco: string;
  edadMinima: number;
  edadNino: number;
  inicial: string;
  visible: boolean;
  webpageBarco: string;
  anioConstruccion: number;
  largo: string;
  ancho: string;
  velocidad: string;
  profundidad: string;
  // ENDPOINT VARS
  estrellas: string;
  itinerarios: any;

}
