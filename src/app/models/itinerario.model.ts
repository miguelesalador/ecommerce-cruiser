export class Itinerario {
  codigo: string;
  estado: number;
  fechaSalida: string;
  fechaRetorno: string;
  tipoItinerario: any;
  disponibles: number;
  bloqueados: number;
  confirmados: number;
  listaEspera: number;
  enEspera: number;
  extension: number;
  observacionOperaciones: string;
  dias: number;
  fechaItinerario: string;
}
