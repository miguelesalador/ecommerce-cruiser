export class Search {
  month: number;
  year: number;
  adults: number;
  children: number;
}
