import { Routes, RouterModule } from "@angular/router";

import { LoginFormComponent } from "app/login/login-form/login-form.component";







const routes: Routes = [
    { path: '', redirectTo: '/search', pathMatch: 'full' },
    { path: 'login', component: LoginFormComponent },
    { path: 'layout', loadChildren: 'app/layout/layout.module#LayoutModule' }
];


export const appRouting = RouterModule.forRoot(routes);