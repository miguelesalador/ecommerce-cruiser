import { TestBed, inject } from '@angular/core/testing';

import { BarcoService } from './barco.service';

describe('BarcoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BarcoService]
    });
  });

  it('should be created', inject([BarcoService], (service: BarcoService) => {
    expect(service).toBeTruthy();
  }));
});
