import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
// import { Observable } from 'rxjs/Observable';
import { Observable } from 'rxjs';


import { environment } from "../../../environments/environment";
import { ApiSettingsModule } from '../../modules/api-settings/api-settings.module';
import { Barco } from '../../models/barco.model';
import { Search } from '../../models/search.model';
import { SlimLoadingBarService } from "ng2-slim-loading-bar";


@Injectable()
export class BarcoService {

  private headers = new Headers({ 'Content-Type': 'application/json' });
  private APi_URL: string = environment.API_URL;


  constructor(private http: Http,
    private apiSettings: ApiSettingsModule,
    private ngProgress: SlimLoadingBarService) {
  }

  /**
   * Get Cruises
   * @param search
   * @returns {Observable<Barco[]>}
   */
  public getCruises(search: Search): Observable<Barco[]> {
    const result: Barco[] = [];

    this.ngProgress.start();
    let url = `${this.APi_URL}busqueda/salidas/`
      + search.adults + '/'
      + search.children + '/'
      + search.month + '/'
      + search.year;
    return this.http.get(url, { headers: this.headers })

      .map((response: any) => {

        return response.json() as Barco[]
      })
      .do(() => this.ngProgress.complete())
      .catch(this.handleError);
  }

  /**
   * New Barco from other source
   * @param param
   * @returns {Barco}
   */
  public newBarcoFrom(param): Barco {
    const barco = new Barco();
    barco.codigo = param.codigo;
    barco.estado = param.estado;
    barco.nombre = param.nombre;
    barco.categoria = param.categoria;
    barco.descripcion = param.descripcion;
    barco.capacidad = param.capacidad;
    barco.imagen = param.imagen;
    barco.tipoImagen = param.tipoImagen;
    barco.nombreImagen = param.nombreImagen;
    barco.logo = param.logo;
    barco.tipoLogo = param.tipoLogo;
    barco.nombreLogo = param.nombreLogo;
    barco.tipoBarco = param.tipoBarco;
    barco.edadMinima = param.edadMinima;
    barco.edadNino = param.edadNino;
    barco.inicial = param.inicial;
    barco.visible = param.visible;
    barco.webpageBarco = param.webpageBarco;
    barco.anioConstruccion = param.anioConstruccion;
    barco.largo = param.largo;
    barco.ancho = param.ancho;
    barco.velocidad = param.velocidad;
    barco.profundidad = param.profundidad;
    // ENDPOINT VARS
    barco.estrellas = param.estrellas;
    barco.itinerarios = param.itinerarios;
    return barco;
  }

  /**
   * Handle error request
   * @param error
   * @returns {Promise<never>}
   */
  private handleError(error: any): Observable<any> {
    return Observable.throw(error.json() || error);
  }
}
