import { Pipe, PipeTransform } from '@angular/core';
import { Barco } from "../../../models/barco.model";


class FilterConfig {
  value: any;
  property: string;
  nestedProperty?: boolean
}


@Pipe({
  name: 'cruisesFilter'
})
export class CruisesFilterPipe implements PipeTransform {

  transform(cruises: Barco[], args?: FilterConfig[]): any {

    let filtering = cruises;

    args.forEach((arg: FilterConfig) => {
      filtering = this.filterBy(filtering, arg);
    })

    // if (filtering.length > 0) {
    return filtering
    // } else return cruises;
  }


  /**
   * 
   * @param cruises the array of cruises
   * @param arg and configuration object to filter
   */
  private filterBy(cruises: Barco[], arg: FilterConfig): Barco[] {

    if (arg.value === undefined) { return cruises };

    let filtered: Barco[];

    if (!arg.nestedProperty) {
      filtered = cruises.filter((cruiser: Barco) => cruiser[arg.property] === arg.value);
    } else {
      filtered = cruises.filter((cruiser: Barco) => {
        if (cruiser.itinerarios.filter((ite) => ite[arg.property] == arg.value).length > 0) {
          return cruiser
        }
      });
    }


    return filtered;
  }



}
