import { Pipe, PipeTransform } from '@angular/core';
import { Barco } from "../../../models/barco.model";

class FilterConfig {
  property: string;
  operator: string;
  type?: string;
}


@Pipe({
  name: 'orderBy'
})
export class OrderByPipe implements PipeTransform {

  transform(value: any[], args?: FilterConfig): any {

    
    if (value === undefined || value.length === 0) {
      return []
    } else if (args.type === 'string') {

      return this.orderByString(value, args)


    } else {

      return this.orderBy(value, args)
    }

  }


  private orderBy(Cruises: Barco[], config: FilterConfig): Barco[] {

    switch (config.operator) {

      case '>':
        return Cruises.sort((a: Barco, b: Barco) => a[config.property] - b[config.property])


      case '<':
        return Cruises.sort((a: Barco, b: Barco) => b[config.property] - a[config.property])

      default:
        break;
    }

  }



  private orderByString(Cruises: Barco[], config: FilterConfig): Barco[] {
    

    switch (config.operator) {

      case '>':
        return Cruises.sort((a: Barco, b: Barco) => {
          if (a[config.property].charAt(0) < b[config.property].charAt(0)) {
            return -1;
          } else if (a[config.property].charAt(0) > b[config.property].charAt(0)) {
            return 1;
          } else {
            return 0;
          }

        })


      case '<':
        return Cruises.sort((a: Barco, b: Barco) => {
          if (a[config.property].charAt(0) > b[config.property].charAt(0)) {
            return -1;
          } else if (a[config.property].charAt(0) <  b[config.property].charAt(0)) {
            return 1;
          } else {
            return 0;
          }

        })

      default:
        break;
    }

  }

}
