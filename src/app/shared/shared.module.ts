import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";


import { CruisesFilterPipe } from './pipes/cruises-filter.pipe';
import { OrderByPipe } from './pipes/order-by.pipe';
import { SlimLoadingBarModule } from "ng2-slim-loading-bar";
import { DatepickerModule } from "ngx-bootstrap";


@NgModule({
  imports: [
    CommonModule,
    SlimLoadingBarModule.forRoot(),
    FormsModule,
    DatepickerModule.forRoot(),
    ReactiveFormsModule

  ],
  declarations: [CruisesFilterPipe, OrderByPipe],

  exports: [CruisesFilterPipe,
    OrderByPipe,
    SlimLoadingBarModule,
    FormsModule,
    DatepickerModule,
    ReactiveFormsModule]
})
export class SharedModule { }
