import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginFormComponent } from "app/modules/login/login-form/login-form.component";
import { SharedModule } from "app/modules/shared/shared.module";

@NgModule({
  imports: [
    CommonModule,SharedModule
  ],
  declarations: [
    LoginFormComponent
  ]
})
export class LoginModule { }
