import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';


import { FormsModule } from '@angular/forms';
import { DatepickerModule } from 'ngx-bootstrap';
import { HttpModule } from '@angular/http';
import { ApiSettingsModule } from './modules/api-settings/api-settings.module';
import { BarcoService } from './services/barco/barco.service';
import { BusyModule } from 'angular2-busy';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


//routings
import { appRouting } from "./app.routing";

// shared module

import { SharedModule } from "./shared/shared.module";

//core app module
import { CoreModule } from "./core/core.module";

//features modules
import { LoginModule } from "app/login/login.module";

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ApiSettingsModule,
    HttpModule,
    BrowserAnimationsModule,
    BusyModule,
    CoreModule,
    SharedModule,
    LoginModule,
    appRouting

  ],
  providers: [
    BarcoService
  ],
  bootstrap: [AppComponent]
})

export class AppModule {
}
