import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { LayoutMenuComponent } from './components/layout-header/layout-menu/layout-menu.component';
import { LayoutFooterComponent } from './components/layout-footer/layout-footer.component';
import { LayoutHeaderComponent } from './components/layout-header/layout-header.component';
import { LayoutContactComponent } from './components/layout-footer/layout-contact/layout-contact.component';


@NgModule({
  imports: [
    CommonModule
  ],
  exports: [
    LayoutHeaderComponent,
    LayoutFooterComponent,

  ],
  declarations: [

    LayoutContactComponent,
    LayoutFooterComponent,
    LayoutHeaderComponent,
    LayoutMenuComponent]
})
export class CoreModule { }
