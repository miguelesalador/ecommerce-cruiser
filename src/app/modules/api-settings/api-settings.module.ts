import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: []
})
export class ApiSettingsModule {
  /**
   * API DOMAIN
   * @returns {string}
   * @constructor
   */
  public get API_DOMAIN(): string {
    return 'https://192.168.0.214:8181/';
  }



  /**
   * API PATH
   * @returns {string}
   * @constructor
   */
  public get API_PATH(): string {
    return 'galavail-rest/webresources/';
  }

  /**
   * PHOTO PATH
   * @returns {string}
   * @constructor
   */
  public get PHOTO_PATH(): string {
    return 'upload/fleet/';
  }
}
