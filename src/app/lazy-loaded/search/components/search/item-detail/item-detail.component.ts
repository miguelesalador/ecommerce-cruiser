import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Location } from '@angular/common';
import 'rxjs/add/operator/switchMap';

import { Search } from '../../../../../models/search.model';


@Component({
  selector: 'app-item-detail',
  templateUrl: './item-detail.component.html',
  styleUrls: ['./item-detail.component.css']
})
export class ItemDetailComponent implements OnInit {

  @Input() searchObject: Search;


  constructor(private route: ActivatedRoute,
    private location: Location) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.searchObject = params as Search;
    });
  }

}
