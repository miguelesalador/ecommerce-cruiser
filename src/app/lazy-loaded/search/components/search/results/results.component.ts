import { Component, Input, OnInit } from '@angular/core';
import { Barco } from '../../../../../models/barco.model';
import { ApiSettingsModule } from '../../../../../modules/api-settings/api-settings.module';

@Component({
  selector: 'gal-cruiser-card',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})

export class ResultsComponent implements OnInit {

  @Input() cruise: Barco;

  constructor(public apiSettings: ApiSettingsModule) {
  }

  /**
   * ngOnInit re-write
   */
  ngOnInit() {
    console.log()
  }

}
