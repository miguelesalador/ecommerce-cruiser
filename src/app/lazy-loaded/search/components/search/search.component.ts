import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import * as moment from 'moment';
import { Search } from '../../../../models/search.model';
import { BarcoService } from '../../../../services/barco/barco.service';
import { Barco } from '../../../../models/barco.model';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})

export class SearchComponent implements OnInit {

  itemDate: Date = new Date();
  today = moment(this.itemDate).format('MMM, YYYY');
  dateString = '';
  show = false;
  minMode = 'month';
  datepickerMode = 'month';
  adults = 0;
  children = 0;
  searchObject: Search;
  cruisesItineraries: Barco[] = [];
  public filterByStar: number;
  public filterByType: number;
  public filterByDays: number;
  public daysAvalibels: number[] = [];
  order: any;
  public priceOrder: string = '>';
  public orderByProperty: string = 'capacidad';
  public filterStringName: string;

  public daysTrip: Set<number> = new Set();


  public selectFilterBy: any[] = [
    { name: 'rating', property: 'categoria' },
    { name: 'name', property: 'nombre' },
    { name: 'price', property: 'precio' },
  ]

  public shipTypes: string[] = ['Cruise Ships', 'Catamaran', 'Small Yacht', 'Motor Sailer', 'Diving Cruise', 'Motor Yacht'];
  public stars: any[] = [
    { value: 1, icon: '★' },
    { value: 2, icon: '★★' },
    { value: 3, icon: '★★★' },
    { value: 4, icon: '★★★★' },
    { value: 5, icon: '★★★★★' }
  ]


  constructor(private route: Router,
    private barcoService: BarcoService) {
  }

  /**
 * ngOnInit re-write
 */
  ngOnInit(): void {
  }


  public getDate(event): void {
    this.show = false;
    this.itemDate = event;
    this.dateString = moment(event).format('MMM, YYYY');
  }

  /**
   * Search Cruises
   */
  public searchCruise(): void {
    const searchNew = new Search();
    searchNew.month = this.itemDate.getMonth() + 1;
    searchNew.year = this.itemDate.getFullYear();
    searchNew.adults = this.adults;
    searchNew.children = this.children;
    this.searchObject = searchNew;
    this.barcoService.getCruises(searchNew).
      subscribe((cruises: Barco[]) => {


        this.cruisesItineraries = this.buildItems(cruises)
        this.setDays(this.cruisesItineraries);


      });
  }



  /**
   * Build cruise list by itinerary
   * @param cruises
   * @returns {Array}
   */
  public buildItems(cruises: Barco[]): Barco[] {

    const results = [];
    for (let cruise of cruises) {
      for (let itinerary of cruise.itinerarios) {
        const auxCruise = this.barcoService.newBarcoFrom(cruise);
        auxCruise.itinerarios = itinerary;
        results.push(auxCruise);
      }
    }
    return results;
  }




  public orderBy(event): void {
    debugger
    console.log(event)
    this.filterStringName = this.setPropertyType(this.order.property)


    this.orderByProperty = this.order.property;
    this.priceOrder = this.priceOrder == '>' ? '<' : '>';

  }


  private setPropertyType(propertyName: string): string {
    switch (propertyName) {
      case 'nombre':
        return 'string'
      case 'cantidad':
        return 'number'
      case 'categoria':
        return 'number'


      default:
        break;
    }
  }

  private setDays(cruiser: Barco[]): void {

    cruiser.forEach((cruiser: Barco) => {
      cruiser.itinerarios
        .forEach((ite: any) => {
          this.daysTrip.add(ite.dias)
        })

    })


    this.daysTrip.forEach((val) => this.daysAvalibels.push(val))

  }

}
