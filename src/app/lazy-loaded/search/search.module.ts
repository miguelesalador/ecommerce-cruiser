import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { SearchComponent } from './components/search/search.component';
import { ResultsComponent } from './components/search/results/results.component';
import { ItemDetailComponent } from './components/search/item-detail/item-detail.component';

//shared module
import { SharedModule } from "app/modules/shared/shared.module";

@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ],
  declarations: [
    SearchComponent,
    ResultsComponent,
    ItemDetailComponent

  ],
  exports: [
    SearchComponent,
    ResultsComponent
  ]
})
export class SearchModule { }
