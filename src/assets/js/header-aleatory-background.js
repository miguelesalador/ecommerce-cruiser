jQuery(document).ready(function() {
  <!--Background superior aleatorio-->
  var images = ['2.jpg', '3.jpg', '5.jpg', '7.jpg', '8.jpg'];
  $('header.headtop').animate({opacity: 0}, 'slow', function() {
    $(this).
      css({
        'background-image': 'url(assets/images/backgrounds/home/' +
        images[Math.floor(Math.random() * images.length)] + ')',
      }).
      animate({opacity: 1});
  });

  $('select#selchild').change(function() {
    $('#selsmall option').nextAll().remove();
    $('#small-childs').hide();
    if ($(this).val() !== '0') {
      $('#small-childs').show();
    }
    else {
      $('#small-childs').hide();
      $('#selsmall').val('0');
    }

    var topnumber = $(this, 'option').val();
    var option = '';
    var list = [];
    for (var i = 1; i <= topnumber; i++) {
      list.push(i);
    }
    for (var i = 0; i < list.length; i++) {
      option += '<option value="' + list[i] + '">' + list[i] + '</option>';
    }
    $('#selsmall').append(option);
  });

  $('select#selboat').change(function() {
    if ($(this).val() !== '0') {
      $('#selcategory, #cruise-type').prop('disabled', true).val('2');
    }
    else {
      $('#selcategory, #cruise-type').prop('disabled', false).val('1');
    }
    ;
  });

  $('select#cruise-lenght').change(function() {
    if ($(this).val() == 'more') {
      window.location.href = 'search-tours-pt1-more.html';
    }
  });

  $('a.plane').click(function() {
    $.fancybox.open({
      type: 'iframe',
      width: 350,
      padding: 5,
      href: 'pop-up-plane.html',
    });
    return false;
  });

  $('a.add-to-cart, .promo-container ul li a').click(function() {
    $('.promo-container table').hide();
    $(this).parent().parent().parent().find('table').show();
    $('html, body').animate({
      scrollTop: $(this).offset().top - 180,
    }, 500);
    return false;
  });
});
